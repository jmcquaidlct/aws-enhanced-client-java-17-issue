package aws.enhanced.client.java.issue;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbAttribute;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbImmutable;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbSortKey;


@DynamoDbBean
@DynamoDbImmutable(builder = TestItem.Builder.class)
@EqualsAndHashCode
@ToString
@Builder(builderClassName = "Builder")
public class TestItem {

    public static final String partitionKey = "pK";
    public static final String indexKey = "otherId";
    public static final String indexName = "testIndex";


    @Getter(onMethod = @__( {@DynamoDbPartitionKey, @DynamoDbAttribute(partitionKey)}))
    private final String id;


    @Getter(onMethod = @__( {@DynamoDbSortKey}))
    private final String name;

    @Getter(onMethod = @__( {@DynamoDbAttribute("address")}))
    private final String address;

    public TestItem(String id, String name, String address){
        this.id = id;
        this.name = name;
        this.address = address;
    }
}
